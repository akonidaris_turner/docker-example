FROM node:8.6.0-slim

WORKDIR app

# Install dependencies
COPY package.json .
RUN npm install
RUN ls
# Import source files
COPY . .

# This final command will be executed when our Image is run as a Container
# CMD ["node", "app.js"]